
package com.khemra.shopping.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.khemra.shopping.R;
import com.khemra.shopping.adapters.MyListAdapter;
import com.khemra.shopping.models.MyListData;
import com.khemra.shopping.utilities.RegisterActionBar;

public class  CategoriesActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView categories_title;

    private RecyclerView recyclerView;
    private MyListAdapter myListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        // Customize Toolbar
        toolbar = findViewById(R.id.toolbar);
        categories_title = findViewById(R.id.tv_categories_toolbar);
        categories_title.setText("Categories");
        RegisterActionBar.registerSupportToolbar(this, toolbar);

        // Recycler Data
        MyListData[] myListData = new MyListData[]{
                new MyListData("Drinks", R.drawable.ic_baseline_shopping_cart_24),
                new MyListData("Foods", R.drawable.ic_baseline_shopping_cart_24),
                new MyListData("Industry Hardware", R.drawable.ic_baseline_shopping_cart_24),
        };

        recyclerView = findViewById(R.id.categories_recycler);
        myListAdapter = new MyListAdapter(getBaseContext());
        myListAdapter.addMyListAdapter(myListData);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(myListAdapter);

    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
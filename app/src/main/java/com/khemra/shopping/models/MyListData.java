package com.khemra.shopping.models;

public class MyListData {
    private String label;
    private int imgId;

    public MyListData(String label, int imgId){
        this.label = label;
        this.imgId = imgId;
    }

    public String getLabel() {
        return label;
    }

    public int getImgId() {
        return imgId;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }

}

package com.khemra.shopping.models;

public class MyGridData {
    private String description;
    private String price;
    private int imgId;

    public MyGridData(String description, String price, int imgId) {
        this.description = description;
        this.price = price;
        this.imgId = imgId;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }

    public int getImgId() {
        return imgId;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public void setPrice(String price) {
        this.price = price;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }
}


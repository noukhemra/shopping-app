package com.khemra.shopping.models.login;

import com.google.gson.annotations.SerializedName;

public class Header{

	@SerializedName("result")
	private boolean result;

	@SerializedName("serverTimestamp")
	private long serverTimestamp;

	@SerializedName("statusCode")
	private int statusCode;

	public void setResult(boolean result){
		this.result = result;
	}

	public boolean isResult(){
		return result;
	}

	public void setServerTimestamp(long serverTimestamp){
		this.serverTimestamp = serverTimestamp;
	}

	public long getServerTimestamp(){
		return serverTimestamp;
	}

	public void setStatusCode(int statusCode){
		this.statusCode = statusCode;
	}

	public int getStatusCode(){
		return statusCode;
	}
}
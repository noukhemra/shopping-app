package com.khemra.shopping.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khemra.shopping.R;
import com.khemra.shopping.models.MyGridData;

public class MyGridAdapter extends RecyclerView.Adapter<MyGridAdapter.ViewHolder>{
    private MyGridData[] gridData;
    private Context mContext;

    public MyGridAdapter (Context mContext) {
        this.mContext = mContext;
    }

    public void addMyGridAdapter(MyGridData[] gridData){
        this.gridData = gridData;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public MyGridAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View gridItem = layoutInflater.inflate(R.layout.grid_view, parent, false);
        ViewHolder viewHolder = new ViewHolder(gridItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyGridAdapter.ViewHolder holder, int position) {
        final MyGridData myGridData = gridData[position];
        holder.txtDescription.setText(gridData[position].getDescription());
        holder.txtPrice.setText(gridData[position].getPrice());
        holder.imageView.setImageResource(gridData[position].getImgId());
    }

    @Override
    public int getItemCount() { return gridData != null ? gridData.length : 0; }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView txtPrice;
        public TextView txtDescription;
        public  ViewHolder (View view){
            super(view);
            this.imageView = (ImageView)view.findViewById(R.id.img_GridView);
            this.txtDescription = (TextView)view.findViewById(R.id.tv_description_gridView);
            this.txtPrice = (TextView)view.findViewById(R.id.tv_price_gridView);
        }
    }
}

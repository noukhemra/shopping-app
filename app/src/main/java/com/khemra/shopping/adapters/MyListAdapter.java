package com.khemra.shopping.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.khemra.shopping.R;
import com.khemra.shopping.models.MyListData;

public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder> {

    private MyListData[] listdata;
    private Context mContext;

    public MyListAdapter(Context mContext) { this.mContext = mContext; }

    public MyListAdapter(MyListData[] listdata) {
        this.listdata = listdata;
    }

    public void addMyListAdapter(MyListData[] listdata){
        this.listdata = listdata;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_view,parent,false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final MyListData myListData = listdata[position];

        Log.d("=====>data_List:",""+listdata[position]);
        holder.textView.setText(listdata[position].getLabel());
        holder.imageView.setImageResource(listdata[position].getImgId());

    }

    @Override
    public int getItemCount() {
        return listdata != null ? listdata.length : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;
        public ViewHolder( View itemView) {
            super(itemView);
            this.imageView = (ImageView)itemView.findViewById(R.id.imgView_label);
            this.textView = (TextView)itemView.findViewById(R.id.tv_label);
        }
    }
}

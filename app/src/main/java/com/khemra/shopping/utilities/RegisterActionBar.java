package com.khemra.shopping.utilities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.khemra.shopping.activities.ForgotPasswordActivity;
import com.khemra.shopping.R;

public class RegisterActionBar {
    public static void registerSupportToolbar(AppCompatActivity applicationContext, Toolbar toolbar){
        applicationContext.setSupportActionBar(toolbar);
        if (applicationContext.getSupportActionBar()!=null){
            applicationContext.getSupportActionBar().setTitle("");
            applicationContext.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_bxs_chevron_left_circle);
            applicationContext.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
}

package com.khemra.shopping.utilities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.DimenRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.khemra.shopping.R;
import com.khemra.shopping.activities.CategoriesActivity;
import com.khemra.shopping.activities.ForgotPasswordActivity;
import com.khemra.shopping.adapters.MyGridAdapter;
import com.khemra.shopping.models.MyGridData;

public class HomeFragment extends Fragment {

    private MyGridAdapter myGridAdapter;
    private RecyclerView recyclerView;
    View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container,false);
        LinearLayout tabCategories = (LinearLayout) view.findViewById(R.id.tab_categories);
        tabCategories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CategoriesActivity.class);
                startActivity(intent);
            }
        });
        MyGridData[]myGridData = new MyGridData[]{
                new MyGridData("VENDING Machine","US $50.00",R.drawable.ic_giant_hypermarket_seeklogo),
                new MyGridData("VENDING Machine","US $50.00",R.drawable.ic_giant_hypermarket_seeklogo),
                new MyGridData("VENDING Machine","US $50.00",R.drawable.ic_giant_hypermarket_seeklogo),
                new MyGridData("VENDING Machine","US $50.00",R.drawable.ic_giant_hypermarket_seeklogo),
                new MyGridData("VENDING Machine","US $50.00",R.drawable.ic_giant_hypermarket_seeklogo),
                new MyGridData("VENDING Machine","US $50.00",R.drawable.ic_giant_hypermarket_seeklogo),
                new MyGridData("VENDING Machine","US $50.00",R.drawable.ic_giant_hypermarket_seeklogo),
                new MyGridData("VENDING Machine","US $50.00",R.drawable.ic_giant_hypermarket_seeklogo),
                new MyGridData("VENDING Machine","US $50.00",R.drawable.ic_giant_hypermarket_seeklogo),
                new MyGridData("VENDING Machine","US $50.00",R.drawable.ic_giant_hypermarket_seeklogo),
        };

        recyclerView = view.findViewById(R.id.home_recycler);
        myGridAdapter = new MyGridAdapter(getContext());
        myGridAdapter.addMyGridAdapter(myGridData);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        recyclerView.setAdapter(myGridAdapter);
        return view;
    }
}
